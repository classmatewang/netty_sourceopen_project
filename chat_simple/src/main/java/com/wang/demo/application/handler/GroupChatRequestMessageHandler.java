package com.wang.demo.application.handler;

import com.wang.demo.protocol.message.GroupChatRequestMessage;
import com.wang.demo.protocol.message.GroupChatResponseMessage;
import com.wang.demo.service.factory.GroupSessionFactory;
import io.netty.channel.Channel;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;

import java.util.List;

/**
 * @author: Jeffrey
 * @date: 2022/01/20/8:34
 * @description:
 */
@ChannelHandler.Sharable
public class GroupChatRequestMessageHandler extends SimpleChannelInboundHandler<GroupChatRequestMessage> {
    @Override
    protected void channelRead0(ChannelHandlerContext ctx, GroupChatRequestMessage msg) throws Exception {

        /*所有在线群成员的channels*/
        List<Channel> channels = GroupSessionFactory.getGroupSession().getMembersChannel(msg.getGroupName());
        channels.forEach((channel -> {
                    channel.writeAndFlush(new GroupChatResponseMessage(msg.getGroupName(),msg.getFrom(), msg.getContent()));
                }));

    }
}
