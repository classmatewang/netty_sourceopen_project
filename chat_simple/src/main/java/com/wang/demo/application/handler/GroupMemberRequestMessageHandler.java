package com.wang.demo.application.handler;

import com.wang.demo.protocol.message.GroupJoinRequestMessage;
import com.wang.demo.protocol.message.GroupMembersRequestMessage;
import com.wang.demo.protocol.message.GroupMembersResponseMessage;
import com.wang.demo.service.factory.GroupSessionFactory;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;

import java.util.Set;

/**
 * <p>获取群成员</p>
 * @author: Jeffrey
 * @date: 2022/01/20/8:32
 * @description:
 */
@ChannelHandler.Sharable
public class GroupMemberRequestMessageHandler extends SimpleChannelInboundHandler<GroupMembersRequestMessage> {

    @Override
    protected void channelRead0(ChannelHandlerContext ctx, GroupMembersRequestMessage msg) throws Exception {
        String groupName = msg.getGroupName();
        Set<String> members = GroupSessionFactory.getGroupSession().getMembers(groupName);
        if (members == null){
            ctx.writeAndFlush(new GroupMembersResponseMessage(null));
        }else {
            ctx.writeAndFlush(new GroupMembersResponseMessage(members));
        }
    }
}
