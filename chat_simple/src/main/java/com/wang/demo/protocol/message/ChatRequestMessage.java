package com.wang.demo.protocol.message;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * <p>聊天建立请求消息
 */

@EqualsAndHashCode(callSuper = true)
@Data
@ToString(callSuper = true)
public class ChatRequestMessage extends Message {

    /**
     * 聊天内容
     */
    private String content;

    /**
     * 目的用户
     */
    private String to;

    /**
     * 源用户
     */
    private String from;

    public ChatRequestMessage() {
    }

    public ChatRequestMessage(String from, String to, String content) {
        this.from = from;
        this.to = to;
        this.content = content;
    }

    /**
     * @return 返回本消息的消息类型
     */
    @Override
    public int getMessageType() {
        return ChatRequestMessage;
    }
}
