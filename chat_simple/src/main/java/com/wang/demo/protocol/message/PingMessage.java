package com.wang.demo.protocol.message;

/**
 * <p>心跳数据包，定期发送解决连接假死
 */
public class PingMessage extends Message {


    @Override
    public int getMessageType() {
        return PingMessage;
    }
}
