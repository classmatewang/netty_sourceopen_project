package com.wang.demo.protocol.codec;

import io.netty.handler.codec.LengthFieldBasedFrameDecoder;

/**
 * @author: Jeffrey
 * @date: 2022/01/18/9:20
 * @description: 封装器 直接指定参数就不用再复杂的写很长了
 */
public class ProcotoFrameDecoder extends LengthFieldBasedFrameDecoder {

    public ProcotoFrameDecoder(){
        this(1024,
                12,
                4,
                0,0);
    }


    public ProcotoFrameDecoder(int maxFrameLength, int lengthFieldOffset, int lengthFieldLength, int lengthAdjustment, int initialBytesToStrip) {
        super(maxFrameLength, lengthFieldOffset, lengthFieldLength, lengthAdjustment, initialBytesToStrip);
    }
}
