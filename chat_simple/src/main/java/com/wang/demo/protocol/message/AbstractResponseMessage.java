package com.wang.demo.protocol.message;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * <p>响应消息的抽象父类
 */

@EqualsAndHashCode(callSuper = true)
@Data
@ToString(callSuper = true)
public abstract class AbstractResponseMessage extends Message {

    /**
     * 封装操作结果是否成功
     */
    private boolean success;

    /**
     * 封装传递的信息
     */
    private String reason;

    public AbstractResponseMessage() {
    }

    public AbstractResponseMessage(boolean success, String reason) {
        this.success = success;
        this.reason = reason;
    }
}
