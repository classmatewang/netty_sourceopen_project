package com.wang.demo.service.impl;

import com.wang.demo.service.Session;
import io.netty.channel.Channel;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author: Jeffrey
 * @date: 2022/01/21/10:09
 * @description: 整个application 的会话
 */
public class SessionMemoryImpl implements Session {

    /**
     * username:channel
     */
    private final Map<String, Channel> usernameChannelMap = new ConcurrentHashMap<>();

    /**
     * channel:username
     */
    private final Map<Channel, String> channelUsernameMap = new ConcurrentHashMap<>();

    /**
     * channel:(key:value)
     */
    private final Map<Channel,Map<String,Object>> channelAttributesMap = new ConcurrentHashMap<>();

    @Override
    public void bind(Channel channel, String username) {
        usernameChannelMap.put(username, channel);
        channelUsernameMap.put(channel, username);
        /*给每一个连接，配置一个attribute 空间*/
        channelAttributesMap.put(channel, new ConcurrentHashMap<>());
    }

    @Override
    public void unbind(Channel channel) {
        String username = channelUsernameMap.remove(channel);
        usernameChannelMap.remove(username);
        channelAttributesMap.remove(channel);
    }

    @Override
    public Object getAttribute(Channel channel, String key) {
        return channelAttributesMap.get(channel).get(key);
    }

    @Override
    public void setAttribute(Channel channel, String key, Object value) {
        channelAttributesMap.get(channel).put(key, value);
    }

    @Override
    public Channel getChannel(String username) {
        return usernameChannelMap.get(username);
    }
}
