package com.wang.demo.service.factory;

import com.wang.demo.service.Session;
import com.wang.demo.service.impl.SessionMemoryImpl;

/**
 * @author: Jeffrey
 * @date: 2022/01/21/10:15
 * @description:
 */
public class SessionFactory {

    private static Session session =new SessionMemoryImpl();

    public static Session getSession() {
        return session;
    }

}
