package com.wang.demo.service.factory;

import com.wang.demo.service.GroupSession;
import com.wang.demo.service.impl.GroupSessionMemoryImpl;

/**
 * @author: Jeffrey
 * @date: 2022/01/22/9:09
 * @description:
 */
public class GroupSessionFactory {

    private static GroupSession session = new GroupSessionMemoryImpl();

    public static GroupSession getGroupSession() {
        return session;
    }
}
