package com.wang.demo.service.factory;

import com.wang.demo.service.UserService;
import com.wang.demo.service.impl.UserServiceImpl;

/**
 * @author: Jeffrey
 * @date: 2022/01/21/10:06
 * @description:
 */
public class UserServiceFactory {

    private static UserService userService = new UserServiceImpl();

    public static UserService getUserService() {
        return userService;
    }
}
