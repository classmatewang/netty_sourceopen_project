package com.wang.demo.service;

/**
 * @author: Jeffrey
 * @date: 2022/01/21/9:43
 * @description:
 */
public interface UserService {

    /**
     * 用户登录
     * @param username 用户名
     * @param password 密码
     * @return 是否允许登录
     */
    boolean login(String username,String password);
}
