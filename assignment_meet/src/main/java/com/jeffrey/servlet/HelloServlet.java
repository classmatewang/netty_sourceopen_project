package com.jeffrey.servlet;

import com.base.servlet.constant.HttpResponseStatus;
import com.base.servlet.http.HttpServlet;
import com.base.servlet.http.HttpServletRequest;
import com.base.servlet.http.HttpServletResponse;
import com.jeffrey.service.UserService;
import com.jeffrey.service.UserServiceFactory;

import java.nio.charset.StandardCharsets;

/**
 * <p>实现一个接口，这个接口能进行逻辑判读，比对成功（登陆成功） 返回true 失败返回false
 * <p>输入：前端页面传递的username 和 password(数据)
 * <p>输出：判定的结果（数据）
 * 进行数据处理
 * @author: Jeffrey
 * @date: 2022/01/23/11:10
 * @description:
 */
public class HelloServlet extends HttpServlet{

    /**
     * http 协议常用两种方法就是get post
     * @param request
     * @param response
     */
    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) {
        super.doGet(request, response);
    }

    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) {
        String username = (String) request.getParameter("username");
        String password = (String) request.getParameter("password");
        if ("".equals(username) && username == null){
            /*报错*/
        }

        if("".equals(password) && password ==null ){
            /*报错*/
        }
        UserService userService = UserServiceFactory.getUserService();
        if (userService.login(username,password)){
            response.setContentType("text/html;charset=utf-8");
            response.setStatus(HttpResponseStatus.SC_OK);
            response.getOutputStream().write("<html><h1>泥嚎！</h1></html>".getBytes(StandardCharsets.UTF_8));
        }else{
            response.setContentType("text/html;charset=utf-8");
            response.setStatus(HttpResponseStatus.SC_NOT_FOUND);
            response.getOutputStream().write("你戳了！崩溃吗！".getBytes(StandardCharsets.UTF_8));
        }
    }
}

