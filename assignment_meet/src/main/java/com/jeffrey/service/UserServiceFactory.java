package com.jeffrey.service;

/**
 * @author: Jeffrey
 * @date: 2022/01/24/19:54
 * @description: 工厂设计模式
 */
public class UserServiceFactory {

    /**
     * 内存空间中有一个就行
     * 我们的需求要求是同一个
     *
     */
    private static UserService userService = new UserServiceDatabaseImpl();

    public static UserService getUserService(){
        return userService;
    }
}
