package com.jeffrey.service;

/**
 * @author: Jeffrey
 * @date: 2022/01/24/19:41
 * @description:
 */
public interface UserService {

    /**
     * 登录逻辑实现
     * @param username 用户名
     * @param password 密码
     * @return 登录成功true  登录失败false
     */
    public boolean login(String username, String password);


}
