package com.jeffrey.service;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author: Jeffrey
 * @date: 2022/01/24/19:43
 * @description:
 */
public class UserServiceImpl implements UserService{

    /**
     * ConcurrentHashMap
     * HashMap
     */
    private Map<String,String> map = new ConcurrentHashMap<>();

     {
        /*初始化 在对象创建时加载*/
        map.put("zhangsan","123456");
        map.put("lisi","123456");
    }

    @Override
    public boolean login(String username, String password) {
         String mPassword = map.get(username);
         if ( mPassword == null){
            return false;
         }
         return password.equals(mPassword);
    }
}
