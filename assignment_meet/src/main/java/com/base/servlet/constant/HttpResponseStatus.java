package com.base.servlet.constant;

/**
*@author: Jeffrey
*@date: 2022/01/24/9:46
*@description:
*/
public enum HttpResponseStatus {

    SC_OK(200,"OK"),
    SC_NOT_FOUND(404,"NOT FOUND"),
    SC_INTERNAL_SERVER_ERROR(500,"SERVER ERROR");

    private int code;
    private String message;

    HttpResponseStatus(int code, String message) {
        this.code = code;
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }
}
