package com.base.servlet.constant;

/**
 * @author: Jeffrey
 * @date: 2022/01/23/9:19
 * @description:
 */
public class HttpRequestHeaderKeyConstant {


    public static final String CONTENT_TYPE = "content-type";

    public static final String CONTENT_LENGTH = "content-length";

    public static final String CONNECTION = "connection";

    public static final String HOST ="host";

    public static final String REFERER ="referer";

}
