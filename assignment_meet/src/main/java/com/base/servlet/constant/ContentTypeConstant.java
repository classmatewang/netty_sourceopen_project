package com.base.servlet.constant;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author: Jeffrey
 * @date: 2022/01/24/9:36
 * @description:
 */
public class ContentTypeConstant {

    /*ContentType 信息，用于存储文件后缀名和*/
    private static Map<String,String> content_type = new ConcurrentHashMap<>();

    static {
        content_type.put("txt","text/html;charset=utf-8");
        content_type.put("css","text/css;charset=utf-8");
        content_type.put("js","text/js;charset=utf-8");
        content_type.put("html","text/html;charset=utf-8");
        content_type.put("json","text/json;charset=utf-8");
        content_type.put("png","application/x-png");
    }

    public static String get(String suffix){
        return content_type.get(suffix);
    }
}
