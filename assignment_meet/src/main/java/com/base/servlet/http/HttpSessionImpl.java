package com.base.servlet.http;

import java.nio.channels.Channel;

/**
 * @author: Jeffrey
 * @date: 2022/01/23/10:02
 * @description:
 */
public class HttpSessionImpl extends AbstractAttribute implements HttpSession {
    @Override
    public Object getAttribute(String key) {
        return null;
    }

    @Override
    public void setAttribute(String key, Object value) {

    }

    @Override
    public void bind(Channel channel, String username) {

    }

    @Override
    public void unbind(Channel channel) {

    }

    @Override
    public Channel getChannel(String username) {
        return null;
    }
}
