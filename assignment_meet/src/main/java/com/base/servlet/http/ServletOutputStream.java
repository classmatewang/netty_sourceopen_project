package com.base.servlet.http;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.ByteBufAllocator;

/**
 * @author: Jeffrey
 * @date: 2022/01/24/15:09
 * @description:
 */
public class ServletOutputStream {

    private ByteBuf byteBuf = ByteBufAllocator.DEFAULT.buffer();

    public void write(byte[] bytes){
        byteBuf.writeBytes(bytes);
    }

    public ByteBuf getByteBuf() {
        return byteBuf;
    }
}
