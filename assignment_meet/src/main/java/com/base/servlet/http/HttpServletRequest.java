package com.base.servlet.http;

import java.io.InputStream;
import java.util.Map;
import java.util.Set;

/**
 * @author: Jeffrey
 * @date: 2022/01/22/21:06
 * @description:
 */
public interface HttpServletRequest extends Http {

    /**
     * 获取Http请求的Method类型
     * @return method str
     */
    public String getMethod();

    /**
     * 获取全路径
     * @return uri path
     */
    public String getRequestURI();

    /**
     * 获取除去域名/端口号的URL
     * @return url path
     */
    public String getRequestUrl();

    /**
     * 获取会话对象
     * @return HttpSession 对象
     */
    public HttpSession getSession();

    /**
     * 获取Http 请求的头部
     * @return Http 头部映射
     */
    public Map<String,String> getHeaders();

    /**
     * 根据var 获取指定的Http头部请求参数
     * @param var key
     * @return value
     */
    public String getHeader(String var);

    /**
     * 获取Http 请求数据流
     * @return http请求体的数据流
     */
    public InputStream getInputStream();

    /**
     * 获取数据传输类型
     * @return ContentTypeConstant类型
     */
    public String getContentType();

    /**
     * 获取url 中封装的数据
     * @param key 键
     * @return value
     */
    public Object getParameter(String key);

    /**
     * 获取所有的parameter 的键的Set
     * @return HashSet
     */
    public Set<String> getParameterNames();

}
