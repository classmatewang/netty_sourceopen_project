package com.base.servlet.http;

/**
 * @author: Jeffrey
 * @date: 2022/01/22/21:51
 * @description:
 */
public interface Http {

    /**
     * map中获取值
     * @param key 键
     * @return 值
     */
    public Object getAttribute(String key);

    /**
     * 设置键值对
     * @param key 键
     * @param value 值
     */
    public void setAttribute(String key,Object value);
}
