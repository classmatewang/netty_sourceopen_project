package com.base.servlet.http;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

/**
 * @author: Jeffrey
 * @date: 2022/01/22/21:15
 * @description:
 */
public abstract class HttpServlet extends AbstractAttribute implements Http{

    public void init(HttpServletRequest request,HttpServletResponse response) throws IOException {

        switch (request.getMethod().toLowerCase()){
            case "get": doGet(request,response); break;
            case "post": doPost(request, response);break;
            case "delete": doDelete(request,response);break;
            case "put": doPut(request,response);break;
        }

        HttpServletResponseWrapper responseWrapper = (HttpServletResponseWrapper) response;
        String s = response.getOutputStream().getByteBuf().toString(StandardCharsets.UTF_8);
        responseWrapper.setBody(s);
        responseWrapper.setContentLength(s.getBytes(StandardCharsets.UTF_8).length);
        responseWrapper.setBodyBytes(s.getBytes(StandardCharsets.UTF_8));
    }

    public void doGet(HttpServletRequest request,HttpServletResponse response){

    }

    public void doPost(HttpServletRequest request,HttpServletResponse response){
    }

    public void doPut(HttpServletRequest request,HttpServletResponse response){

    }

    public void doDelete(HttpServletRequest request,HttpServletResponse response){

    }

}
