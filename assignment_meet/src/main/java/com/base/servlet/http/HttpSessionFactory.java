package com.base.servlet.http;

/**
 * @author: Jeffrey
 * @date: 2022/01/23/10:02
 * @description:
 */
public class HttpSessionFactory {

    private static HttpSession session = new HttpSessionImpl();

    public static HttpSession getSession(){
        return session;
    }
}
