package com.base.servlet.http;

import com.base.servlet.constant.HttpResponseStatus;

import java.io.IOException;
import java.io.PrintWriter;

/**
 * @author: Jeffrey
 * @date: 2022/01/22/21:06
 * @description:
 */
public interface HttpServletResponse {

    public void setHeader(String name, String value);

    public void setStatus(HttpResponseStatus status);

    public String getCharacterEncoding();

    public String getContentType();

    public ServletOutputStream getOutputStream();

    public PrintWriter getWriter() throws IOException;

    public void setCharacterEncoding(String charset);

    public void setContentLength(int len);

    public void setContentType(String type);


}
