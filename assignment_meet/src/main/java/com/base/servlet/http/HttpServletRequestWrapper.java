package com.base.servlet.http;

import com.base.servlet.message.HttpMessage;
import io.netty.channel.Channel;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.Map;
import java.util.Set;

import static io.netty.handler.codec.http.HttpHeaderNames.CONTENT_TYPE;

/**
 * @author: Jeffrey
 * @date: 2022/01/23/8:18
 * @description:
 */
public class HttpServletRequestWrapper extends AbstractAttribute implements HttpServletRequest {

    private String method;
    private String url;
    private String uri;
    private Map<String,String> headers;
    private Channel socketChannel;
    private final HttpMessage message;
    private Map<String,Object> parameters;
    private HttpSession httpSession;

    public HttpServletRequestWrapper(String method,
                                     String url,
                                     String uri,
                                     Map<String, String> headers,
                                     Channel socketChannel,
                                     HttpMessage message,
                                     Map<String, Object> parameters,
                                     HttpSession httpSession) {
        this.method = method;
        this.url = url;
        this.uri = uri;
        this.headers = headers;
        this.socketChannel = socketChannel;
        this.message = message;
        this.parameters = parameters;
        this.httpSession = httpSession;
    }

    @Override
    public String getMethod() {
        return this.method;
    }

    @Override
    public String getRequestURI() {
        return this.uri;
    }

    @Override
    public String getRequestUrl() {
        return this.url;
    }


    @Override
    public HttpSession getSession() {
        return this.httpSession;
    }

    @Override
    public Map<String, String> getHeaders() {
        return this.headers;
    }

    @Override
    public String getHeader(String var) {
        return headers.get(var);
    }

    @Override
    public InputStream getInputStream() {
        ByteArrayInputStream inputStream = new ByteArrayInputStream(message.getBytes());
        return inputStream;
    }

    @Override
    public String getContentType() {
        return this.headers.get(CONTENT_TYPE);
    }

    @Override
    public Object getParameter(String key) {
        return this.parameters.get(key);
    }

    @Override
    public Set<String> getParameterNames() {
        return this.parameters.keySet();
    }

}
