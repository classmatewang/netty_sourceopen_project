package com.base.servlet.http;

import java.nio.channels.Channel;

/**
 * @author: Jeffrey
 * @date: 2022/01/22/21:25
 * @description:
 */
public interface HttpSession extends Http {

    /**
     * 绑定会话
     * @param channel 哪个 channel 要绑定会话
     * @param username 会话绑定用户
     */
    void bind(Channel channel, String username);

    /**
     * 解绑会话
     * @param channel 哪个 channel 要解绑会话
     */
    void unbind(Channel channel);

    /**
     * 根据用户名获取 channel
     * @param username 用户名
     * @return channel
     */
    Channel getChannel(String username);
}
