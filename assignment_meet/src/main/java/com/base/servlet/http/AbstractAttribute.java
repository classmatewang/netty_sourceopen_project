package com.base.servlet.http;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author: Jeffrey
 * @date: 2022/01/23/8:19
 * @description:
 */

public abstract class AbstractAttribute {

    private Map<String,Object> attribute = new ConcurrentHashMap<>();

    public Object getAttribute(String key){
        return attribute.get(key);
    }

    public void setAttribute(String key,Object value){
        attribute.put(key,value);
    }
}
