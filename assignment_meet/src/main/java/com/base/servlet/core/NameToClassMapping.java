package com.base.servlet.core;

import com.base.servlet.http.HttpServlet;
import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * @author: Jeffrey
 * @date: 2022/01/23/22:40
 * @description:
 */
@AllArgsConstructor
@Data
public class NameToClassMapping {

    private String name;

    private Class<HttpServlet> aClass;


}
