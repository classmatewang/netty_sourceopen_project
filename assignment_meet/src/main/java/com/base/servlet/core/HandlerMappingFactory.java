package com.base.servlet.core;

/**
 * @author: Jeffrey
 * @date: 2022/01/23/22:23
 * @description:
 */
public class HandlerMappingFactory {

    private static HandlerMapping handlerMapping = new HandlerMapping();

    /**
     * @return 默认配置的xml
     */
    public static HandlerMapping getHandlerMapping(){
        return handlerMapping;
    }

}
