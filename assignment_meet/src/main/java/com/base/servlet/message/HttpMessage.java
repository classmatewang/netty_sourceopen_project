package com.base.servlet.message;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.nio.charset.StandardCharsets;
import java.util.Map;

/**
 * @author: Jeffrey
 * @date: 2022/01/22/22:29
 * @description:
 */
@NoArgsConstructor
@AllArgsConstructor
@Data
public class HttpMessage {


    /**
     * 请求行和响应行
     */
    private String line;

    /**
     * 请求头和响应头
     */
    private Map<String,String> header;

    /**
     * 分隔符
     */
    private final  byte[] separator = "\r\n".getBytes(StandardCharsets.UTF_8);

    /**
     * 请求体和响应体
     */
    private String body;

    /**
     * 请求体数据流
     */
    private byte[] bytes;
}
